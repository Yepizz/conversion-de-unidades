﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1LOC Base
//*************************
// Bloque de Código fuente:
// Clase Volumen
// Breve: incluye todos los métodos de de conversiones
//de entre el sistema métrico y del sistema imperial, de 
//unidades de VOLUMEN
//*************************
//TODO: terminarlo.
//UNDONE
//HACK
namespace Volumen
{
    public class Volumen
    {
        /// <summary>
        /// Constructor para hacer conversiones de Volumen
        /// entre sistema métrico e imperial.
        /// </summary>
        public Volumen()
        {

        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// CENTIMETROS CUBICOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="cm">Los CENTIMETROS CUBICOS que deseo convertir
        /// a CENTIMETROS CUBICOS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en CENTIMETROS CUBICOS ingresados por el usuario</returns>
        public decimal convCmCubAPlgCub(double cm)
        {
            return decimal.Parse((cm / 16.387).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los CENTIMETROS CUBICOS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a CENTIMETROS CUBICOS</param>
        /// <returns>Nos devolverá las CENTIMETROS CUBICOS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario</returns>
        public decimal convPlgCubACmCub(double plg)
        {
            return decimal.Parse((plg * 16.387).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// DECIMETROS CUBICOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="dcm">Los DECIMETROS CUBICOS que deseo convertir
        /// a PULGADAS CUBICAS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en DECIMETROS CUBICOS ingresados por el usuario</returns>
        public decimal convDcmCubAPlgCub(double dcm)
        {
            return decimal.Parse((dcm * 61.024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los DECIMETROS CUBICOS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a DECIMETROS CUBICOS</param>
        /// <returns>Nos devolverá los DECIMETROS CUBICOS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario</returns>
        public decimal convPlgCubADcmCub(double plg)
        {
            return decimal.Parse((plg / 61.024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// METROS CUBICOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS CUBICOS que deseo convertir
        /// a PULGADAS CUBICAS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en METROS CUBICOS ingresados por el usuario</returns>
        public decimal convMtCubAPlgCub(double mt)
        {
            return decimal.Parse((mt * 61024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los METROS CUBICOS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a METROS CUBICOS</param>
        /// <returns>Nos devolverá los METROS CUBICOS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario</returns>
        public decimal convPlgCubAMtCub(double plg)
        {
            return decimal.Parse((plg * 0.000016387).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// LITROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="lt">Los LITROS que deseo convertir
        /// a PULGADAS CUBICAS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en LITROS ingresados por el usuario</returns>
        public decimal convLtAPlgCub(double lt)
        {
            return decimal.Parse((lt * 61.024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los LITROS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a LITROS</param>
        /// <returns>Nos devolverá los LITROS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario</returns>
        public decimal convPlgCubALt(double plg)
        {
            return decimal.Parse((plg / 61.024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// HECTOLITROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="hect">Los HECTOLITROS que deseo convertir
        /// a PULGADAS CUBICAS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en HECTOLITROS ingresados por el usuario</returns>
        public decimal convHectAPlgCub(double hect)
        {
            return decimal.Parse((hect * 6102).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los HECTOLITROS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a HECTOLITRO</param>
        /// <returns>Nos devolverá los HECTOLITROS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario.</returns>
        public decimal convPlgCubAHect(double plg)
        {
            return decimal.Parse((plg / 6102).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de DECIMETROS CUBICOS ingresados
        /// por parametro a CENTIMETROS CUBICOS.
        /// </summary>
        /// <param name="cm">CENTIMETROS CUBICOS que se convertirán en
        /// DECIMETROS CUBICOS</param>
        /// <returns>DECIMETROS CUBICOS correspondientes a la conversión</returns>
        public decimal convCmCubADcmCub(double cm)
        {
            return decimal.Parse((cm / 1000).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de CENTIMETROS CUBICOS ingresados
        /// por parametro a DECIMETROS CUBICOS.
        /// </summary>
        /// <param name="dcm">DECIMETROS CUBICOS que se convertirán en
        /// CENTIMETROS CUBICOS</param>
        /// <returns>CENTIMETROS CUBICOS correspondientes a la conversión</returns>
        public decimal convDcmCubACmCub(double dcm)
        {
            return decimal.Parse((dcm * 1000).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de METROS CUBICOS ingresados
        /// por parametro a DECIMETROS CUBICOS.
        /// </summary>
        /// <param name="dcm">DECIMETROS CUBICOS que se convertirán en
        /// METROS CUBICOS</param>
        /// <returns>METROS CUBICOS correspondientes a la conversión</returns>
        public decimal convDcmCubAMtCub(double dcm)
        {
            return decimal.Parse((dcm / 1000000).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de DECIMETROS CUBICOS ingresados
        /// por parametro a METROS CUBICOS.
        /// </summary>
        /// <param name="mt">METROS CUBICOS que se convertirán en
        /// DECIMETROS CUBICOS</param>
        /// <returns>DECIMETROS CUBICOS correspondientes a la conversión</returns>
        public decimal convMtCubADcmCub(double mt)
        {
            return decimal.Parse((mt * 1000000).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de METROS CUBICOS ingresados
        /// por parametro a LITROS.
        /// </summary>
        /// <param name="mt">LITROS que se convertirán en
        /// METROS CUBICOS</param>
        /// <returns>METROS CUBICOS correspondientes a la conversión</returns>
        public decimal convLtAMtCub(double mt)
        {
            return decimal.Parse((mt / 1000).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de LITROS ingresados
        /// por parametro a METROS CUBICOS.
        /// </summary>
        /// <param name="mt">METROS CUBICOS que se convertirán en
        /// LITROS</param>
        /// <returns>LITROS correspondientes a la conversión</returns>
        public decimal convMtCubALt(double mt)
        {
            return decimal.Parse((mt * 1000).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTOLITROS ingresados
        /// por parametro a LITROS.
        /// </summary>
        /// <param name="lt">LITROS que se convertirán en
        /// HECTOLITROS</param>
        /// <returns>HECTOLITROS correspondientes a la conversión</returns>
        public decimal conLtAHect(double lt)
        {
            return decimal.Parse((lt / 1000).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de LITROS ingresados
        /// por parametro a HECTOLITROS.
        /// </summary>
        /// <param name="hect">HECTOLITROS que se convertirán en
        /// LITROS</param>
        /// <returns>LITROS correspondientes a la conversión</returns>
        public decimal convHectALt(double hect)
        {
            return decimal.Parse((hect * 1000).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTOLITROS ingresados
        /// por parametro a METROS CUBICOS.
        /// </summary>
        /// <param name="mt">METROS CUBICOS que se convertirán en
        /// HECTOLITROS</param>
        /// <returns>HECTOLITROS correspondientes a la conversión</returns>
        public decimal convMtCubAHect(double mt)
        {
            return decimal.Parse((mt * 10).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de METROS CUBICOS ingresados
        /// por parametro a HECTOLITROS.
        /// </summary>
        /// <param name="hect">HECTOLITROS que se convertirán en
        /// METROS CUBICOS</param>
        /// <returns>METROS CUBICOS correspondientes a la conversión</returns>
        public decimal convHectAMtCub(double hect)
        {
            return decimal.Parse((hect / 10).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PIE CUBICOS ingresados
        /// por parametro a HECTOLITRO.
        /// </summary>
        /// <param name="hect">HECTOLITROS que se convertirán en
        /// PIE CUBICOS</param>
        /// <returns>PIE CUBICO correspondientes a la conversión</returns>
        public decimal convHectAPieCub(double hect)
        {
            return decimal.Parse((hect * 3.531).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTOLITRO ingresados
        /// por parametro a PIES CUBICOS.
        /// </summary>
        /// <param name="pie">PIE CUBICO que se convertirán en
        /// HECTOLITRO</param>
        /// <returns>HECTOLITRO correspondientes a la conversión</returns>
        public decimal convPieCubAHect(double pie)
        {
            return decimal.Parse((pie / 3.531).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS ingresados
        /// por parametro a HECTOLITROS.
        /// </summary>
        /// <param name="hect">HECTOLITROS que se convertirán en
        /// ONZAS</param>
        /// <returns>ONZAS correspondientes a la conversión</returns>
        public decimal conHectAOz(double hect)
        {
            return decimal.Parse((hect * 3333.3333333333).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTOLITRO ingresados
        /// por parametro a ONZAS LIQUIDAS IMPERIALES.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS que se convertirán en
        /// HECTOLITRO</param>
        /// <returns>HECTOLITRO correspondientes a la conversión</returns>
        public decimal convOzAHect(double oz)
        {
            return decimal.Parse((oz / 3333.3333333333).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PINTA ingresados
        /// por parametro HECTOLITROS.
        /// </summary>
        /// <param name="hect">HECTOLITROS  que se convertirán en
        /// PINTA</param>
        /// <returns>PINTA correspondientes a la conversión</returns>
        public decimal convHectAPinta(double hect)
        {
            return decimal.Parse((hect * 176).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTOLITRO ingresados
        /// por parametro a PINTAS.
        /// </summary>
        /// <param name="pinta">PINTAS que se convertirán en
        /// HECTOLITRO</param>
        /// <returns>HECTOLITRO correspondientes a la conversión</returns>
        public decimal convPintaAHect(double pinta)
        {
            return decimal.Parse((pinta / 176).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de GALONES ingresados
        /// por parametro a HECTOLITROS.
        /// </summary>
        /// <param name="hect">HECTROLITROS que se convertirán en
        /// GALONES</param>
        /// <returns>GALONES correspondientes a la conversión</returns>
        public decimal convHectAGalon(double hect)
        {
            return decimal.Parse((hect * 21.997).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTOLITRO ingresados
        /// por parametro a GALONES.
        /// </summary>
        /// <param name="galon">GALONES que se convertirán en
        /// HECTOLITRO</param>
        /// <returns>HECTOLITRO correspondientes a la conversión</returns>
        public decimal convGalonAHect(double galon)
        {
            return decimal.Parse((galon / 21.997).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS EEUU ingresados
        /// por parametro a HECTOLITROS.
        /// </summary>
        /// <param name="hect">HECTOLITROS que se convertirán en
        /// ONZAS LIQUIDAS EEUU</param>
        /// <returns>HECTOLITRO correspondientes a la conversión</returns>
        public decimal convHectAOzLiqEu(double hect)
        {
            return decimal.Parse((hect * 3381).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTOLITRO ingresados
        /// por parametro a ONZAS LIQUIDAS EEUU.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS EEUU que se convertirán en
        /// HECTOLITRO</param>
        /// <returns>HECTOLITRO correspondientes a la conversión</returns>
        public decimal convOzLiqEuAHect(double oz)
        {
            return decimal.Parse((oz / 3381).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES CUBICOS ingresados
        /// por parametro a PULGADAS CUBICOS.
        /// </summary>
        /// <param name="plg">PULGADAS CUBICAS que se convertirán en
        /// PIES CUBICOS</param>
        /// <returns>PIES CUBICOS correspondientes a la conversión</returns>
        public decimal convPlgCubAPieCub(double plg)
        {
            return decimal.Parse((plg / 1728).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUBICAS ingresados
        /// por parametro a PIES CUBICOS.
        /// </summary>
        /// <param name="pie">PIE CUBICO que se convertirán en
        /// PULGADAS CUBICAS</param>
        /// <returns>PULGADAS CUBICAS correspondientes a la conversión</returns>
        public decimal convPieCubAPlgCub(double pie)
        {
            return decimal.Parse((pie * 1728).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS ingresados
        /// por parametro a PULGADAS CUBICAS.
        /// </summary>
        /// <param name="plg">PULGADAS CUBICAS CUBICO que se convertirán en
        /// ONZAS LIQUIDAS</param>
        /// <returns>ONZAS LIQUIDAS correspondientes a la conversión</returns>
        public decimal convPlgCubAOzLiq(double plg)
        {
            return decimal.Parse((plg / 1.734).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUBICAS ingresados
        /// por parametro a ONZAS LIQUIDAS.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS que se convertirán en
        /// PULGADAS CUBICAS</param>
        /// <returns>PULGADAS CUBICAS correspondientes a la conversión</returns>
        public decimal convOzLiqAPlgCub(double oz)
        {
            return decimal.Parse((oz * 1.734).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PINTAS ingresados
        /// por parametro a PULGADAS CUBICAS.
        /// </summary>
        /// <param name="plg">PULGADAS CUBICAS que se convertirán en
        /// PINTAS</param>
        /// <returns>PINTAS correspondientes a la conversión</returns>
        public decimal convPlgCubAPinta(double plg)
        {
            return decimal.Parse((plg / 34.677).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUBICAS ingresados
        /// por parametro a PINTAS.
        /// </summary>
        /// <param name="pinta">PINTAS que se convertirán en
        /// PULGADAS CUBICAS</param>
        /// <returns>PULGADAS CUBICAS correspondientes a la conversión</returns>
        public decimal convPintaAPlgCub(double pinta)
        {
            return decimal.Parse((pinta * 34.677).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de GALONES ingresados
        /// por parametro a PULGADAS CUBICAS.
        /// </summary>
        /// <param name="plg">PULGADAS CUBICO que se convertirán en
        /// GALONES</param>
        /// <returns>GALONES correspondientes a la conversión</returns>
        public decimal convPlgCubAGalon(double plg)
        {
            return decimal.Parse((plg / 277).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUBICAS ingresados
        /// por parametro a GALONES.
        /// </summary>
        /// <param name="galon">GALONES que se convertirán en
        /// PULGADAS CUBICAS</param>
        /// <returns>PULGADAS CUBICAS correspondientes a la conversión</returns>
        public decimal convGalonAPlgCub(double galon)
        {
            return decimal.Parse((galon * 277).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de GALONES ingresados
        /// por parametro a PIES CUBICOS.
        /// </summary>
        /// <param name="pie">PIE CUBICO que se convertirán en
        /// GALONES</param>
        /// <returns>GALONES correspondientes a la conversión</returns>
        public decimal convPieCubAGalon(double pie)
        {
            return decimal.Parse((pie * 6.229).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES CUBICOS ingresados
        /// por parametro a GALONES.
        /// </summary>
        /// <param name="galon">GALONES que se convertirán en
        /// PIES CUBICOS</param>
        /// <returns>PIES CUBICOS correspondientes a la conversión</returns>
        public decimal convGalonAPieCub(double galon)
        {
            return decimal.Parse((galon / 6.229).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS ingresados
        /// por parametro a PIES CUBICOS.
        /// </summary>
        /// <param name="pie">PIE CUBICO que se convertirán en
        /// ONZAS LIQUIDAS</param>
        /// <returns>ONZAS LIQUIDAS correspondientes a la conversión</returns>
        public decimal convPieCubAOzLiq(double pie)
        {
            return decimal.Parse((pie * 997).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES CUBICOS ingresados
        /// por parametro a ONZAS LIQUIDAS.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS que se convertirán en
        /// PIES CUBICOS</param>
        /// <returns>PIES CUBICOS correspondientes a la conversión</returns>
        public decimal convOzLiqAPieCub(double oz)
        {
            return decimal.Parse((oz / 997).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PINTAS ingresados
        /// por parametro a ONZAS LIQUIDAS.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS que se convertirán en
        /// PINTAS</param>
        /// <returns>PINTAS correspondientes a la conversión</returns>
        public decimal convOzLiqAPinta(double oz)
        {
            return decimal.Parse((oz / 20).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS ingresados
        /// por parametro a PINTAS.
        /// </summary>
        /// <param name="pinta">PINTAS que se convertirán en
        /// ONZAS LIQUIDAS</param>
        /// <returns>ONZAS LIQUIDAS correspondientes a la conversión</returns>
        public decimal convPintaAOzLiq(double pinta)
        {
            return decimal.Parse((pinta * 20).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de GALONES ingresados
        /// por parametro a ONZAS LIQUIDAS.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS que se convertirán en
        /// GALONES</param>
        /// <returns>GALONES correspondientes a la conversión</returns>
        public decimal convOzLiqAGalon(double oz)
        {
            return decimal.Parse((oz / 160).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS ingresados
        /// por parametro a GALONES.
        /// </summary>
        /// <param name="galon">GALONES que se convertirán en
        /// ONZAS LOQUIDAS</param>
        /// <returns>ONZAS LIQUIDAS correspondientes a la conversión</returns>
        public decimal convGalonAOzLiq(double galon)
        {
            return decimal.Parse((galon * 160).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUBICAS ingresados
        /// por parametro a ONZAS LIQUIDAS EEUU.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS EEUU que se convertirán en
        /// PULGADAS CUBICAS</param>
        /// <returns>PULGADAS CUBICAS correspondientes a la conversión</returns>
        public decimal convOzLiqEuAPlgCub(double oz)
        {
            return decimal.Parse((oz * 1.805).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS EEUU ingresados
        /// por parametro a PULGADAS CUBICOS.
        /// </summary>
        /// <param name="plg">PULGADAS CUBICAS que se convertirán en
        /// ONZAS LIQUIDAS EEUU</param>
        /// <returns>ONZAS LIQUIDAS EEUU correspondientes a la conversión</returns>
        public decimal convPlgCubAOzLiqEu(double plg)
        {
            return decimal.Parse((plg / 1.805).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES CUBICOS ingresados
        /// por parametro a ONZAS LIQUIDAS EEUU.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS EEUU que se convertirán en
        /// PIES CUBICOS</param>
        /// <returns>PIES CUBICOS correspondientes a la conversión</returns>
        public decimal convOzLiqEuAPieCub(double oz)
        {
            return decimal.Parse((oz / 958).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS QLIUIDAS EEUU ingresados
        /// por parametro a PIES CUBICOS.
        /// </summary>
        /// <param name="pie">PIE CUBICO que se convertirán en
        /// ONZAS LIQUIDAS EEUU</param>
        /// <returns>ONZAS LIQUIDAS EEUU correspondientes a la conversión</returns>
        public decimal convPieCubAOzLiqEu(double pie)
        {
            return decimal.Parse((pie * 958).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS IMPERIALES ingresados
        /// por parametro a ONZAS LIQUIDAS EEUU.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS EEUU que se convertirán en
        /// ONZAS LIQUIDAS IMPERIALES</param>
        /// <returns>ONZAS LIQUIDAS IMPERIALES correspondientes a la conversión</returns>
        public decimal convOzLiqEuAOzLiq(double oz)
        {
            return decimal.Parse((oz * 1.041).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS EEUU ingresados
        /// por parametro a ONZAS LIQUIDAS IMPERIALES.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS IMPERIALES que se convertirán en
        /// ONZAS LIQUIDAS EEUU</param>
        /// <returns>ONZAS LIQUIDAS EEUU correspondientes a la conversión</returns>
        public decimal convOzLiqAOzLiqEu(double oz)
        {
            return decimal.Parse((oz / 1.041).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PINTAS ingresados
        /// por parametro a ONZAS LIQUIDAS EEUU.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS EEUU que se convertirán en
        /// PINTAS</param>
        /// <returns>PINTAS correspondientes a la conversión</returns>
        public decimal convOzLiqEuAPinta(double oz)
        {
            return decimal.Parse((oz / 19.215).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS EEUU ingresados
        /// por parametro a PINTAS.
        /// </summary>
        /// <param name="pinta">PINTAS que se convertirán en
        /// ONZAS LIQUIDAS EEUU</param>
        /// <returns>ONZAS LIQUIDAS EEUU correspondientes a la conversión</returns>
        public decimal convPintaAOzLiqEu(double pinta)
        {
            return decimal.Parse((pinta * 19.215).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de GALONES ingresados
        /// por parametro a ONZAS LIQUIDAS EEUU.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS que se convertirán en
        /// GALONES</param>
        /// <returns>GALONES correspondientes a la conversión</returns>
        public decimal convOzLiqEuAGalon(double oz)
        {
            return decimal.Parse((oz / 154).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS EEUU ingresados
        /// por parametro a GALONES.
        /// </summary>
        /// <param name="galon">GALONES que se convertirán en
        /// ONZAS LIQUIDAS EEUU</param>
        /// <returns>ONZAS LIQUIDAS EEUU correspondientes a la conversión</returns>
        public decimal convGalonAOzLiqEu(double galon)
        {
            return decimal.Parse((galon * 154).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de GALONES EEUU ingresados
        /// por parametro a ONZAS LIQUIDAS EEUU.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS EEUU que se convertirán en
        /// GALONES EEUU</param>
        /// <returns>GALONES EEUU correspondientes a la conversión</returns>
        public decimal convOzLiqEuAGalonEu(double oz)
        {
            return decimal.Parse((oz / 128).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS EEUU ingresados
        /// por parametro a GALONES EEUU.
        /// </summary>
        /// <param name="galon">GALONES EEUU que se convertirán en
        /// ONZAS LIQUIDAS EEUU</param>
        /// <returns>ONZAS LIQUIDAS EEUU correspondientes a la conversión</returns>
        public decimal convGalonEuAOzLiqEu(double galon)
        {
            return decimal.Parse((galon * 128).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PINTAS EEUU ingresados
        /// por parametro a ONZAS LIQUIDAS EEUU.
        /// </summary>
        /// <param name="oz">ONZAS LIQUIDAS EEUU que se convertirán en
        /// PINTAS EEUU</param>
        /// <returns>PINTAS EEUU correspondientes a la conversión</returns>
        public decimal convOzLiqEuAPintaEu(double oz)
        {
            return decimal.Parse((oz / 16).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS LIQUIDAS EEUU ingresados
        /// por parametro a PINTAS EEUU.
        /// </summary>
        /// <param name="pinta">PINTAS EEUU que se convertirán en
        /// ONZAS LIQUIDAS EEUU</param>
        /// <returns>ONZAS LIQUIDAS EEUU correspondientes a la conversión</returns>
        public decimal convPintaEuAOzLiqEu(double pinta)
        {
            return decimal.Parse((pinta * 16).ToString());

        }
    }
}
