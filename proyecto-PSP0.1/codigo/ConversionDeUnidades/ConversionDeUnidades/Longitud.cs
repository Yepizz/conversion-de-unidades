﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1LOC Base
//*************************
// Bloque de Código fuente:
// Clase Longitud
// Breve: incluye todos los métodos de de conversiones
//de entre el sistema métrico y del sistema imperial, de 
//unidades de LONGITUD
//*************************
//TODO: terminarlo
//UNDONE
//HACK
namespace ConversionDeUnidades
{
    //1LOC Base
    public class Longitud
    {
        /// <summary>
        /// Constructor para hacer conversiones de longitud
        /// entre sistema métrico e imperial.
        /// </summary>
        public Longitud()
        {

        }
        /// <summary>
        /// Me permite obtener las PULGADAS que corresponden a los 
        /// MILIMETROS que ingresamos en el parámetro.
        /// </summary>
        /// <param name="mm">Los milimetros que deseo convertir en 
        /// pulgadas</param>
        /// <returns>Las PULGADAS que corresponden a los milimetros 
        /// ingresados</returns>
        public decimal conMmAPlg(double mm)
        {
            return decimal.Parse((mm * 0.03937).ToString());
        }
        /// <summary>
        /// Nos va a permitir obtener Los centimetros a una
        /// conversión a pulgadas
        /// </summary>
        /// <param name="cm">Los centrimetros se convertirán en
        /// pulgadas</param>
        /// <returns>Nos devuelve las pulgadas de los 
        /// centrimetros que hemos introducido</returns>
        public decimal convCmAPlg(double cm)
        {
            return decimal.Parse((cm * 0.3937).ToString());
        }
       /// <summary>
       /// Me permite obtener los MILIMETROS que le corresponderán a 
       /// las pulgadas que queremos obtener en en la entrada por el usuario.
       /// </summary>
       /// <param name="plg">Las PULGADAS que deseo convertir
       /// a MILIMETROS</param>
       /// <returns>Nos devolverá los MILIMETROS que le corresponden
       /// en pulgadas ingresados por el usuario</returns>
        public decimal convPlgAMm(double plg)
        {
            return decimal.Parse((plg / 0.03937).ToString());
        }
        /// <summary>
        /// Me permite obtener los CENTIMETROS que le corresponderán a 
        /// las PULGADAS que queremos obtener en en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS que deseo convertir
        /// a CENTIMETROS</param>
        /// <returns>Nos devolverá los CENTIMETROS que le corresponden
        /// en PULGADAS ingresados por el usuario</returns>
        public decimal convPlgACm(double plg)
        {
            return decimal.Parse((plg / 0.39370).ToString());
        }

        /// <summary>
        /// Me permite obtener los CENTIMETROS que le corresponderán a 
        /// los MILIMETROS que queremos obtener en en la entrada por el usuario.
        /// </summary>
        /// <param name="mm">Los MILIMETROS que deseo convertir
        /// a CENTIMETROS</param>
        /// <returns>Nos devolverá los CENTIMETROS que le corresponden
        /// en MILIMETROS ingresados por el usuario</returns>
        public decimal convMmACm(double mm)
        {
            return decimal.Parse((mm / 10.000).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los MILIMETROS que le corresponderán a los
        /// CENTIMETROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="cm">Los CENTIMETROS que deseo convertir
        /// a MILIMETROS</param>
        /// <returns>Nos devolverá los MILIMETROS que le corresponden
        /// en CENTIMETROS ingresados por el usuario</returns>
        public decimal convCmAMm(double cm)
        {
            return decimal.Parse((cm / 0.10000).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS que le corresponderán a los
        /// METROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS que deseo convertir
        /// a PULGADAS</param>
        /// <returns>Nos devolverá las PULGADAS que le corresponden
        /// en METROS ingresados por el usuario</returns>
        public decimal convMtAPlg(double mt)
        {
            return decimal.Parse((mt / 39.370).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los METROS que le corresponderán a las
        /// PULGADAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS que deseo convertir
        /// a METROS</param>
        /// <returns>Nos devolverá los METROS que le corresponden
        /// en PULGADAS ingresados por el usuario</returns>
        public decimal convPlgAMt(double plg)
        {
            return decimal.Parse((plg * 39.370).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los MILIMETROS que le corresponderán a los
        /// METROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS que deseo convertir
        /// a MILIMETROS</param>
        /// <returns>Nos devolverá los MILIMETROS que le corresponden
        /// en METROS ingresados por el usuario</returns>
        public decimal convMtAMm(double mt)
        {
            return decimal.Parse((mt /0.0010000).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los CENTIMETROS que le corresponderán a los
        /// METROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS que deseo convertir
        /// a CENTIMETROS</param>
        /// <returns>Nos devolverá los CENTIMETROS que le corresponden
        /// en METROS ingresados por el usuario</returns>
        public decimal convMtACm(double mt)
        {
            return decimal.Parse((mt * 100).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los METROS que le corresponderán a los
        /// CENTIMETROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="cm">Los CENTIMETROS que deseo convertir
        /// a METROS</param>
        /// <returns>Nos devolverá los METROS que le corresponden
        /// en CENTIMETROS ingresados por el usuario</returns>
        public decimal convCmAMt(double cm)
        {
            return decimal.Parse((cm / 100.00).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los PIES que le corresponderán a las
        /// PULGADAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS que deseo convertir
        /// a PIES</param>
        /// <returns>Nos devolverá los PIES que le corresponden
        /// en PULGADAS ingresados por el usuario</returns>
        public decimal convPlgAPie(double plg)
        {
            return decimal.Parse((plg / 12).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS que le corresponderán a los
        /// PIES que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="pie">Los PIES que deseo convertir
        /// a PULGADAS</param>
        /// <returns>Nos devolverá las PULGADAS que le corresponden
        /// en PIES ingresados por el usuario</returns>
        public decimal convPieAPlg(double pie)
        {
            return decimal.Parse((pie * 12).ToString());
        }
       /// <summary>
       /// Permite obtener las MILLAS que el corresponden a las
       /// YARDAS ingresadas por el parametro
       /// </summary>
       /// <param name="yrd">Las YARDAS que se convertirán en MILLAS</param>
       /// <returns>Las MILLAS correspondientes a la conversión</returns>
        public decimal convYrdAMilla(double yrd)
        {
            return decimal.Parse((yrd / 1760).ToString());
        }
        /// <summary>
        /// Permite obtener las YARDAS que el corresponden a las
        /// MILLAS ingresadas por el parametro
        /// </summary>
        /// <param name="milla">Las MILLAS que se convertirán en YARDAS</param>
        /// <returns>Las YARDAS correspondientes a la conversión</returns>
        public decimal convMillaAYrd(double milla)
        {
            return decimal.Parse((milla * 1760).ToString());
        }
        /// <summary>
        /// Permite obtener las MILLAS que el corresponden a las
        /// MILLAS NAUTICAS ingresadas por el parametro
        /// </summary>
        /// <param name="millaNaut">Las MILLAS NAUTICAS que se convertirán en
        /// MILLAS</param>
        /// <returns>Las MILLAS correspondientes a la conversión</returns>
        public decimal convMillaNautAMilla(double millaNaut)
        {
            return decimal.Parse((millaNaut * 1.151).ToString());
        }
        /// <summary>
        /// Permite obtener las MILLAS NAUTICAS que el corresponden a las
        /// MILLAS ingresadas por el parametro
        /// </summary>
        /// <param name="milla">Las MILLAS  que se convertirán en
        /// MILLAS NAUTICAS</param>
        /// <returns>Las MILLAS NAUTICAS correspondientes a la conversión</returns>
        public decimal convMillaAMillaNaut(double milla)
        {
            return decimal.Parse((milla / 1.151).ToString());
        }
        /// <summary>
        /// Permite obtener las PULGADAS que el corresponden a las
        /// YARDAS ingresadas por el parametro
        /// </summary>
        /// <param name="yrd">Las YARDAS  que se convertirán en
        /// PULGADAS</param>
        /// <returns>Las PULGADAS correspondientes a la conversión</returns>
        public decimal convYrdAPlg(double yrd)
        {
            return decimal.Parse((yrd * 36).ToString());
        }
        /// <summary>
        /// Permite obtener las YARDAS que el corresponden a las
        /// PULGADAS ingresadas por el parametro
        /// </summary>
        /// <param name="plg">Las PULGADAS  que se convertirán en
        /// YARDAS</param>
        /// <returns>Las YARDAS correspondientes a la conversión</returns>
        public decimal convPlgAYrd(double plg)
        {
            return decimal.Parse((plg / 36).ToString());
        }
        /// <summary>
        /// Permite obtener las PULGADAS que el corresponden a los
        /// KILOMETROS ingresadas por el parametro
        /// </summary>
        /// <param name="km">Los KILOMETROS  que se convertirán en
        /// PULGADAS</param>
        /// <returns>Las PULGADAS correspondientes a la conversión</returns>
        public decimal convKmAPlg(double km)
        {
            return decimal.Parse((km * 39370).ToString());
        }
        /// <summary>
        /// Permite obtener los KILOMETROS que el corresponden a las
        /// PULGADAS ingresadas por el parametro
        /// </summary>
        /// <param name="plg">Las PULGADAS  que se convertirán en
        /// KILOMETROS</param>
        /// <returns>Los KILOMETROS correspondientes a la conversión</returns>
        public decimal convPlgAKm(double plg)
        {
            return decimal.Parse((plg / 39370).ToString());
        }
        /// <summary>
        /// Permite obtener los PIES que el corresponden a los
        /// KILOMETROS ingresadas por el parametro
        /// </summary>
        /// <param name="km">Los KILOMETROS  que se convertirán en
        /// PIES</param>
        /// <returns>Los PIES correspondientes a la conversión</returns>
        public decimal convKmAPie(double km)
        {
            return decimal.Parse((km * 3281).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de KILOMETROS ingresados
        /// por parametro a PIES.
        /// </summary>
        /// <param name="pie">Los PIES  que se convertirán en
        /// KILOMETROS</param>
        /// <returns>Los KILOMETROS correspondientes a la conversión</returns>
        public decimal convPieAKm(double pie)
        {
            return decimal.Parse((pie / 3281).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de YARDAS ingresados
        /// por parametro a KILOMETROS.
        /// </summary>
        /// <param name="km">Los KILOMETROS  que se convertirán en
        /// YARDAS</param>
        /// <returns>YARDAS correspondientes a la conversión</returns>
        public decimal convKmAYrd(double km)
        {
            return decimal.Parse((km * 1094).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de KILOMETROS ingresados
        /// por parametro a YARDAS.
        /// </summary>
        /// <param name="yrd">YARDAS que se convertirán en
        /// KILOMETROS</param>
        /// <returns>KILOMETROS correspondientes a la conversión</returns>
        public decimal convYrdAKm(double yrd)
        {
            return decimal.Parse((yrd / 1094).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS ingresados
        /// por parametro a KILOMETROS.
        /// </summary>
        /// <param name="km">KILOMETROS que se convertirán en
        /// MILLAS</param>
        /// <returns>MILLAS correspondientes a la conversión</returns>
        public decimal convKmAMilla(double km)
        {
            return decimal.Parse((km / 1.609).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de KILOMETROS ingresados
        /// por parametro a MILLAS.
        /// </summary>
        /// <param name="milla">MILLAS que se convertirán en
        /// KILOMETROS</param>
        /// <returns>KILOMETROS correspondientes a la conversión</returns>
        public decimal convMillaAKm(double milla)
        {
            return decimal.Parse((milla * 1.609).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS NAUTICAS ingresados
        /// por parametro a KILOMETROS.
        /// </summary>
        /// <param name="km">KILOMETROS que se convertirán en
        /// MILLAS NAUTICAS</param>
        /// <returns>MILLAS NAUTICAS correspondientes a la conversión</returns>
        public decimal convKmAMillaNaut(double km)
        {
            return decimal.Parse((km / 1.852).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de KILOMETROS ingresados
        /// por parametro a MILLAS NAUTICAS.
        /// </summary>
        /// <param name="millaNaut">MILLAS NAUTICAS que se convertirán en
        /// KILOMETROS</param>
        /// <returns>KILOMETROS correspondientes a la conversión</returns>
        public decimal convMillaNautAKm(double millaNaut)
        {
            return decimal.Parse((millaNaut * 1.852).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de PIES ingresados
        /// por parametro a METROS.
        /// </summary>
        /// <param name="mt">METROS que se convertirán en
        /// PIES</param>
        /// <returns>PIES correspondientes a la conversión</returns>
        public decimal convMtAPie(double mt)
        {
            return decimal.Parse((mt * 3.281).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de METROS ingresados
        /// por parametro a PIES.
        /// </summary>
        /// <param name="pie">PIES que se convertirán en
        /// METROS</param>
        /// <returns>METROS correspondientes a la conversión</returns>
        public decimal convPieAMt(double pie)
        {
            return decimal.Parse((pie / 3.281).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de YARDAS ingresados
        /// por parametro a METROS.
        /// </summary>
        /// <param name="mt">METROS que se convertirán en
        /// YARDAS</param>
        /// <returns>YARDAS correspondientes a la conversión</returns>
        public decimal convMtAYrd(double mt)
        {
            return decimal.Parse((mt * 1.094).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de METROS ingresados
        /// por parametro a YARDAS.
        /// </summary>
        /// <param name="yrd">YARDAS que se convertirán en
        /// METROS</param>
        /// <returns>METROS correspondientes a la conversión</returns>
        public decimal convYrdAMt(double yrd)
        {
            return decimal.Parse((yrd / 1.094).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS ingresados
        /// por parametro a METROS.
        /// </summary>
        /// <param name="mt">METROS que se convertirán en
        /// MILLAS</param>
        /// <returns>MILLAS correspondientes a la conversión</returns>
        public decimal convMtAMilla(double mt)
        {
            return decimal.Parse((mt / 1609).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de METROS ingresados
        /// por parametro a MILLAS.
        /// </summary>
        /// <param name="milla">MILLAS que se convertirán en
        /// METROS</param>
        /// <returns>METROS correspondientes a la conversión</returns>
        public decimal convMillaAMt(double milla)
        {
            return decimal.Parse((milla * 1609).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS NAUTICAS ingresados
        /// por parametro a METROS.
        /// </summary>
        /// <param name="mt">METROS que se convertirán en
        /// MILLAS NAUTICAS</param>
        /// <returns>MILLAS NAUTICAS correspondientes a la conversión</returns>
        public decimal convMtAMillaNaut(double mt)
        {
            return decimal.Parse((mt / 1852).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de METROS ingresados
        /// por parametro a MILLAS NAUTICAS.
        /// </summary>
        /// <param name="millaNaut">MILLAS NAUTICAS que se convertirán en
        /// METROS</param>
        /// <returns>METROS correspondientes a la conversión</returns>
        public decimal convMillaNautAMt(double millaNaut)
        {
            return decimal.Parse((millaNaut * 1852).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS ingresados
        /// por parametro a MILLAS NAUTICAS.
        /// </summary>
        /// <param name="millaNaut">MILLAS NAUTICAS que se convertirán en
        /// PULGADAS</param>
        /// <returns>PULGADAS correspondientes a la conversión</returns>
        public decimal convMillaNautAPlg(double millaNaut)
        {
            return decimal.Parse((millaNaut * 72913).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS NAUTICAS ingresados
        /// por parametro a PULGADAS.
        /// </summary>
        /// <param name="plg">PULGADAS que se convertirán en
        /// MILLAS NAUTICAS</param>
        /// <returns>MILLAS NAUTICAS correspondientes a la conversión</returns>
        public decimal convPlgAMillaNaut(double plg)
        {
            return decimal.Parse((plg / 72913).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS ingresados
        /// por parametro a PULGADAS.
        /// </summary>
        /// <param name="plg">PULGADAS que se convertirán en
        /// MILLAS</param>
        /// <returns>MILLAS correspondientes a la conversión</returns>
        public decimal convPlgAMilla(double plg)
        {
            return decimal.Parse((plg / 63360).ToString());
        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS ingresados
        /// por parametro a MILLAS.
        /// </summary>
        /// <param name="milla">MILLAS que se convertirán en
        /// PULGADAS</param>
        /// <returns>PULGADAS correspondientes a la conversión</returns>
        public decimal convMillaAPlg(double milla)
        {
            return decimal.Parse((milla * 63360).ToString());
        
        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS ingresados
        /// por parametro a PIES.
        /// </summary>
        /// <param name="pie">PIES que se convertirán en
        /// MILLAS</param>
        /// <returns>MILLAS correspondientes a la conversión</returns>
        public decimal convPieAMilla(double pie)
        {
            return decimal.Parse((pie / 5280).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES ingresados
        /// por parametro a MILLAS.
        /// </summary>
        /// <param name="milla">MILLAS que se convertirán en
        /// PIES</param>
        /// <returns>PIES correspondientes a la conversión</returns>
        public decimal convMillaAPie(double milla)
        {
            return decimal.Parse((milla * 5280).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS NAUTICAS ingresados
        /// por parametro a PIES.
        /// </summary>
        /// <param name="pie">PIES que se convertirán en
        /// MILLAS NAUTICAS</param>
        /// <returns>MILLAS NAUTICAS correspondientes a la conversión</returns>
        public decimal convPieAMillaNaut(double pie)
        {
            return decimal.Parse((pie / 6076).ToString());

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES ingresados
        /// por parametro a MILLAS NAUTICAS.
        /// </summary>
        /// <param name="millaNaut">MILLAS NAUTICAS que se convertirán en
        /// PIES</param>
        /// <returns>PIES correspondientes a la conversión</returns>
        public decimal convMillaNautAPie(double millaNaut)
        {
            return decimal.Parse((millaNaut * 6076).ToString());

        }
    }
}
